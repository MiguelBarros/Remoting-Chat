﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chat
{
    namespace Library
    {
        public interface IClientChat
        {
            void SendClient(string message);
        }
        
        public interface IServerChat
        {
            void AddUser(string nick, string url);
            void SendServer(string nick, string message);
        }
    }
}
