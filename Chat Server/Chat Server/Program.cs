﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Text;
using System.Threading.Tasks;
using Chat.Library;

namespace Chat
{
    namespace Server { 
        
        class Chat_Server : MarshalByRefObject, IServerChat
        {

            private Dictionary<String, String> users = new Dictionary<string, string>();

            public static void Main(String[] args)
            {
                TcpChannel channel = new TcpChannel(8086);
                ChannelServices.RegisterChannel(channel, false);
                RemotingConfiguration.RegisterWellKnownServiceType(typeof(Chat_Server), "ChatServer", WellKnownObjectMode.Singleton);
                System.Console.WriteLine("<enter> para sair...");
                System.Console.ReadLine();
            }

            void IServerChat.AddUser(string nick, string url)
            {
                lock (this)
                {
                    users.Add(nick, url);
                }
            }

            void IServerChat.SendServer(string nick, string message)
            {
                lock(this)
                {
                    foreach (KeyValuePair<String, String> entry in users)
                    {
                        IClientChat client = (IClientChat)Activator.GetObject(typeof(IClientChat), "tcp://" + entry.Value);

                        if (client == null)
                        {
                            System.Console.WriteLine("Could not connect to client {0}", entry.Key);
                            //users.Remove(entry.Key);
                        }
                        else
                        {
                            client.SendClient(nick + " - " + message);
                        }
                    }
                }
                
            }
        }
    }
    
}
