﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Tcp;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Chat.Library;

namespace Chat
{
    namespace Client
    {
        public partial class Form1 : Form
        {
            private IServerChat server;
            private ChatClient client;
            public Form1()
            {
                InitializeComponent();
            }

            private void textBox2_TextChanged(object sender, EventArgs e)
            {

            }

            private void textBox3_TextChanged(object sender, EventArgs e)
            {

            }

            public void receiveMessage(String message)
            {
                this.Chat.Text += message;
            }

            private void button1_Click(object sender, EventArgs e)
            {
                server.SendServer(client.nick, this.MessageBox.Text + "\r\n");
            }

            private void PortBox_TextChanged(object sender, EventArgs e)
            {

            }

            private void Form1_Load(object sender, EventArgs e)
            {

            }

            private void Connect_Click(object sender, EventArgs e)
            {
                String url = "localhost:" + this.PortBox.Text + "/ChatClient";
                TcpChannel channel = new TcpChannel(Int32.Parse(this.PortBox.Text));
                ChannelServices.RegisterChannel(channel, false);
                this.client = new ChatClient(this.NickBox.Text, this);
                RemotingServices.Marshal(this.client, "ChatClient", typeof(ChatClient));
                this.server = (IServerChat)Activator.GetObject(typeof(IServerChat), "tcp://localhost:8086/ChatServer");
                if (server == null)
                {
                    this.Chat.Text += "Could not connect to server\r\n";
                }
                else
                {
                    server.AddUser(this.NickBox.Text, url);
                    this.Connect.Enabled = false;
                }
            }
        }
    }
}
