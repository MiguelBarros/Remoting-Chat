﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Chat.Library;

namespace Chat
{
    namespace Client
    {
       

        class ChatClient : MarshalByRefObject, IClientChat
        {
            delegate void InvokeDelegate(String message);
            public string nick { get; set; }
            //private string url;
            private Form1 form;
            public ChatClient(String nick, Form1 form)
            {
                this.nick = nick;
                //this.url = url;
                this.form = form;
            }
            void IClientChat.SendClient(string message)
            {
                object[] args = new object[1];
                args[0] = message;
                form.BeginInvoke(new InvokeDelegate(form.receiveMessage), args);

            }
        }
    }
    
}
